{
    "id": "473baae0-628b-42ae-8f9e-402e5c8d5ed5",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "322cbae1-d0f0-4ca4-9d66-3de3dfbd4897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "775433ab-7a9d-4372-bd8d-ea089f7ba8cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 166,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "db405623-5365-4d77-be72-e84e6ea8f588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7d04028d-1b53-4dc2-bc9d-8c86a5d6467f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "359f01c2-2a45-4104-97b6-ec9c120be8b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f84495b2-5e58-4014-844b-ebe133961031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ca90a277-5ad7-4f50-b5b5-f47bcb44eaa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4e98770b-0fa5-4900-9a24-7484b2b6f35f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a46994ff-24b8-40e6-b5e3-3358e5585c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "83798c7f-91d0-4495-b24d-fb6ec08a10fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "178b6517-964a-4c25-9abd-882cb92a43db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2d144606-b00f-455f-ac57-f18fe4a3e91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d4a89f80-b859-4571-b3d3-aad5ae0b1e87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 151,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2e4b3722-754c-4c4a-a717-53937474f0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "00ef6541-13c6-4b0e-b9eb-d41ee07cd1c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 156,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "197731e5-a33a-438b-805e-70956efec96d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "14dcfae7-da2b-4c14-a30b-2c002f1914d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 185,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b61e2d3b-0fd3-456e-9390-c6f608353967",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b421755b-80d0-4408-b062-8bf60fe57931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 196,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fb0a4e8b-b2f4-44d6-92e9-2b1b2d59df03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 207,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "95e26f6c-7bc1-4bf5-9f23-85e2a01a8a8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d3fee636-c298-4174-9fbf-f162634c0d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1f774428-6d0e-4ad6-a706-70b53267bb14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "dd66e15f-8b3b-442e-9e97-46b99744a260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9105f4dc-46ed-4e64-bab1-bb9ca63185f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b8628195-746f-4792-845d-213b40abb836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cc47ec7c-48e1-4373-924b-cfe95779bdca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f213d533-009d-45c6-a486-a9f71df45a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b7be7900-f7db-459e-8edb-32e55dc64fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ad15ac78-fa71-4f54-b731-0a1058de73de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "94bb4586-a621-40d0-b7a8-f9c232d881c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2f708089-bac2-43a1-9d3a-dec88aa56eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 22
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2b896124-859f-4993-8753-834b6a8a8f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2a1ac31d-7e3f-4e60-b32e-a5a580f524ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0b5f9fad-ccfc-4baf-811b-5baaa89516fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 141,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "eb39b8ea-4449-415c-a115-0dfe5d10fc4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5159dcc9-8481-4a5d-b9d2-6a6f1dcacf72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7adeb318-608f-41f0-bf4b-2a7acd657064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "22adcfe4-02eb-49bb-b2e2-53e1030d683d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 22
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "437b070f-561e-4e75-8a40-5adf8452d49d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a90311ea-0bcd-4c8d-b3c1-51769edae57d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c2ff738e-a820-4a7e-ae1f-84c38ef00a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 171,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c01abe69-527a-4385-9e89-0c4378d3b438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ed9d296c-fc32-4f7b-989d-b5c3cc0fe03f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "693397a7-f442-498e-8f8a-199f708321ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ec44bc15-a9b9-4920-a5d1-1998fab7c2fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2c054286-5cf6-456a-8ad0-5127b51ff36e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6395df35-8b1b-411d-859f-2be4ffc467b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e176bf1a-6343-419a-8bb5-d4be9870f4ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 152,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f4b7407a-8566-4045-8e1a-2f96e2b65266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cae14c3f-5750-4650-bf8c-ebae2a40ec3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "868390ee-81bb-4918-97ab-5afd2b0d83b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1272dae5-77a5-45f4-b92b-1879d469e12c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "04962f18-bcfa-498f-aac1-518d6f3936bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fd373527-cd0d-4dd2-aeb2-eac804be8aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "56241ab1-a075-4707-b178-5187bba34df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f9981066-3e7d-41dc-b372-87e954b227a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8436f30f-8e06-4aab-989b-8f7ff824762d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "83fd286a-f326-48f2-9d90-5871d65f8ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "23203cb8-f35d-48af-8f70-4cb156924a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 145,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e324bc25-6123-47eb-a668-fad0ecae9101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a28830c1-2fad-49d0-b3c3-8f80e0088dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 127,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "59c88138-fca8-4d5e-8c6a-e20a2656c618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 131,
                "y": 42
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0e19e3a9-55a8-42d1-847f-0f7e55f19e9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c671e022-ee67-4ec1-a418-49efcce1a0c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 133,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2ce5f8e6-84f5-4aeb-8fa1-6c55e8fa1d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "829f3516-d71b-4cda-899e-2e0b0ea5c460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c45db7e8-6b07-47e8-bde8-277c9be2a1bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 151,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e74e1030-0694-4a23-b6c4-b1edd7bb223c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 42
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1c136bd2-7425-49c4-9bef-dfed1fb71233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 108,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7f3e5216-7ce5-405f-be87-1c13593fb906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3319ca1f-cc30-4fbd-bf38-54ae531b5bbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 161,
                "y": 42
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d7822fb0-2ec8-482f-9873-464677af7959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "57ad54bc-d8fd-472d-b7cc-e727ce6b7007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 194,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "96503977-5d09-4776-a063-1525e52886e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 139,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c78e9814-5361-49d2-812e-ff5a76ec971e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "74a8d1e3-75b7-4a4d-8ef6-58d4313d8fde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "33a63869-c05c-4825-b0a3-bf639b488e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ba486ff4-1860-4c83-b146-2a6edd588e4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7bc83f19-cd57-4d4f-a0e8-ca6529519210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e2a208f6-3840-4ccb-8e95-ec4c9e52c091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 141,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "90ef12b7-4772-4a09-bffd-c4b7307143e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 111,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9ec17375-907e-41b9-8264-181ff11e19ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "835d1d6a-be6c-4306-93c6-404b1f9a86c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3efecc72-a1ae-4279-9803-28908d51fa40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b34060aa-4ab1-40d4-a29e-20baf4b30e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e2dada50-f698-4f52-94ba-9b39655b688f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 181,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7d9b4808-9ba6-47b5-b730-68d8fdc54797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d73331fc-94de-47b9-9fd2-19bfe97352f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "61a9da7a-bd5d-4d56-b88e-ee2c756d1ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9fa2c4d8-ed58-436f-b78a-2561c6b35069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f07fe6c9-981a-49e8-ba67-b6409d4d840c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c6168a11-df82-4799-870a-01bbe0c26438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 190,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "219b4ba1-8068-4e2f-8c63-62162411a34c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b5f40325-ad7e-4bb6-b47e-e38b452a6ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 163,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c3fd7c8d-4cbe-48fb-ae9b-31e525cc25e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "bf370eef-7bec-4f7b-84a7-6307278d8e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "7102b8eb-898a-4408-a78d-346bb9519bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "9286d6cc-b01b-4451-9006-e8bdf700fd19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "1aa50edc-4a51-4424-9026-79b9c5104365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "4dc9f4f5-9f3c-41a2-80f5-619e881ffd5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "43f360f2-82e9-4ea9-8c89-547851ae40e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "3fbbc148-cf53-4b23-af57-33a1bb107d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "9f5b030a-5385-4a54-a41f-6bc943b7885b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "b528a7e0-f15f-4629-9211-a021683e665c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "33e98f79-bd3a-474b-90a0-4e742f6b2e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "dbfa6f72-cfb8-430f-901c-2da49b33cdb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "97042686-7eba-4981-a51b-304f34be497d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "a207202a-aa1f-4d07-8b4d-34d68daa0633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "586f3c0b-ea50-4b92-be62-1b37c7a29215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "16f46801-73ce-4e38-9359-61823d7f4376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "7b398afb-9160-469e-848b-01daede96c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "749dffc0-7f2b-4c76-8e79-c7b3ef73bafb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "c640a071-8091-4a6a-96f2-473ff5d82956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "13c318bd-90fc-49c8-bf27-2791df8806b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "459faf87-816b-4e97-a001-f1ee47bf5f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "6a70297e-433f-4ae7-a4ea-1c61c3ff2daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "c894f2cd-dbe1-44f5-9a5c-71e926c26428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "34d7fd07-f256-4f69-bca7-5f4b25e80c80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "8fd461b0-2dd9-4bb4-9bec-fd56ceff0aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "82bbb196-6f07-4628-a4aa-037347d9c39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "2004ac9f-ce20-4957-ae48-56b11e28d12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "140cdf7a-a6a0-4d2f-a5d3-d07f4ca59a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "47ddc7e2-5086-46de-8833-dc11679c6a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "8710faa2-a39e-4e73-8af6-4f898128a9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "167d4b8a-e2da-406c-9175-eeb32d81561e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "adf44009-cee5-405d-887d-4d8eeea0100e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "b1400bf8-9853-4a16-b24f-039646484bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "2f053334-d924-4e0a-90d6-77a332bf864b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "ff0f9d1b-2de7-420c-9af6-18292ae4b660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "9c6449c2-d244-45c0-8f44-c7a7a7dfe66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "4c07aad8-9560-4de7-b7ec-ff4e14d3a692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "0ad6bf4b-64f5-4fec-9887-ff754f3e3e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "ffdae372-c223-4a50-8190-b9ae97ebf309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "d712bfe2-3750-41e9-a7bd-3cf69e044e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "00eee843-3b23-4fdd-90e8-4b79b758faa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "a842ef25-6b22-466b-811d-fdaea05f7cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "418089fb-983d-4d88-a71c-41a985ea2ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "a977e589-c7f6-4b2a-a5c7-12893b1f618b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "7a08db75-e10f-4d2b-ad7f-edf118cce655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "2f854e68-1210-42f6-8069-37463a90539e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "1fdedfe2-9858-4f8e-9478-48ef25fb9d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "88ca7169-febb-484e-a1a3-8a629ba30d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "207016dd-ba28-485f-b844-62fbe914371b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "e5ae819a-8335-4b23-a0fa-d0ddacfdb7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "8b970eb9-c57f-4239-8ba6-a478395486c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "7d9bee85-a670-4559-8c93-3d5885b63792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "2ea77f7f-84ff-4959-8e5d-7e0e9095d77e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "4fc2131f-c522-401c-95f0-3f76eb6413cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "910644e6-47fb-49ea-81f2-17f3fa9a9998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "36a7461f-c24c-41a2-8251-9980c5c675b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "e2bf4f09-0dea-42ff-893c-98750ffb3fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "51a866d8-ce2a-44d1-8c2d-199b61fe131f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "8c31b310-1d58-4c11-854b-c099dcea2e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "f559ee62-c0f1-4389-806d-3c89c1f42f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "83652238-873f-4bbc-bf53-ec057f8a30c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "57d07c0d-da14-46fe-9364-90983db3182d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "14c602d0-7bf3-4326-ad2b-6989f04272b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "a0a6ec26-ff5b-48fc-9a90-be76dc6d292f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "85f809bd-4b2f-4109-8fed-63f0de2f5e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "5d8244f4-1e81-4ef1-beb7-a1cdbc433feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "35ba3574-3c14-4bf3-a347-1f6a3ff45987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "3f258710-ac9f-4027-bed7-3a3f63e8ba87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "350cff3d-bcb1-437b-9bfe-e64ed1c01beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "466d46c5-4b1d-47c5-8a10-7a247380166e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "451f66ca-53fc-460f-8109-df419090a14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}